#!/bin/bash
#Created by:	F@br1x	and	你好😜
######################################

clear
negro='\e[1;30m'
rojo='\e[1;31m'
cyan='\e[1;36m'
verde='\e[1;32m'
a='\e[1;34m'
am='\e[1;33m'
reset='\e[0m'

trap created INT
created() {
	clear
	echo -e "\n\n\n"
	printf "\t${negro}Created by:  ${cyan}F@br1x  ${negro}and   ${cyan}你好😜${reset}\n"
	sleep 4
	echo -e "\n"
	ls
	exit
}

bash 'banner.sh'

sleep 2
while :
do
	echo -e -n "${reset}Para Instalar Todo lo  necesario en Termux\nEscriba la letra [${cyan}s/S${reset}] >>${rojo} "
	read -r opc
	[ "$opc" == "s" ] || [ "$opc" == "S" ] && break
	echo -e "${rojo}Comando Incorrecto${reset}"
done
echo -e "${verde}Iniciando Proceso Para Instalar\nPaquetes en Termux...!${reset}"
sleep 4
echo -e "${a}[${verde}!${a}]${am}Actualizando Repositorios de Termux..."
apt update -y > /dev/null 2>&1
apt upgrade -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando git..."
apt install git -y > /dev/null 2>&1
echo -e "${a}[$verde!${a}]${am}Instalando curl..."
apt install curl -y >/dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando wget..."
apt install wget -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando nano..."
apt install nano -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando vim..."
apt install vim -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando pv..."
apt install pv -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando zip..."
apt install zip -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando unzip..."
apt install unzip -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando htop..."
apt install htop -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando ranger..."
apt install ranger -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando unstable-repo..."
pkg install unstable-repo -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando root-repo..."
pkg install root-repo -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando proot..."
apt install proot -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando aircrack-ng..."
apt install aircrack-ng -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando php..."
apt install php -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando python..."
apt install python -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando python2..."
apt install python2 -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando ruby..."
apt install ruby -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando screen..."
apt install screen -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando tmux..."
apt install tmux -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando hydra..."
apt install hydra -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando clang..."
apt install clang -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando apache2..."
apt install apache2 -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando perl..."
apt install perl -y > /dev/null 2>&1
echo -e "${a}[${verde}!${a}]${am}Instalando axel..."
apt install axel -y > /dev/null 2>&1
sleep 3
printf "\n${verde}Listo Tienes Todos Los Paquetes Básicos\nEn Termux :3...${reset}\n"
sleep 5
source 'banner.sh'
printf "\n"
sleep 3
exit

